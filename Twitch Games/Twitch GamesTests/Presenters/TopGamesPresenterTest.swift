import XCTest
@testable import Twitch_Games

class TopGamesPresenterProtocolMock : NSObject, TopGamesPresenterProtocol {
    var setStartLoadingCalled = false
    var setFinishLoadingCalled = false
    var setTopGamesCalled = false
    var setEmptyStateCalled = false
    var setErrorStateCalled = false
    
    func startLoading(){
        setStartLoadingCalled = true
    }
    
    func finishLoading() {
        setFinishLoadingCalled = true
    }
    
    func setTopGames(topGames: [TopModel]) {
        setTopGamesCalled = true
    }
    
    func setEmptyState() {
        setEmptyStateCalled = true
    }
    
    func setErrorState(errorMessage: String) {
        setErrorStateCalled = true
    }
}

class TopGamesPresenterTest: XCTestCase {
    
    let emptyApiMock = ApiMock(response: HomeResponse(total: 0, top: []))
    let twoGamesApiMock = ApiMock(response: HomeResponse(total: 2, top: [TopModel(), TopModel()]))
    let paginationApiMock = ApiMock(response: HomeResponse(total: 4, top: [TopModel(), TopModel()]))
    
    
    func testShouldSetEmptyStateCalled() {
        //given
        let viewMock = TopGamesPresenterProtocolMock()
        let userPresenterUnderTest = TopGamesPresenter(service: emptyApiMock)
        userPresenterUnderTest.attachView(view: viewMock)
        
        //when
        userPresenterUnderTest.loadData()
        
        //verify
        XCTAssertTrue(viewMock.setEmptyStateCalled)
    }
    
    func testShouldSetGamesCalled() {
        //given
        let viewMock = TopGamesPresenterProtocolMock()
        let userPresenterUnderTest = TopGamesPresenter(service: twoGamesApiMock)
        userPresenterUnderTest.attachView(view: viewMock)
        
        //when
        userPresenterUnderTest.loadData()
        
        //verify
        XCTAssertTrue(viewMock.setTopGamesCalled)
    }
    
    func testShouldStartAndFinishLoadingCalled() {
        //given
        let viewMock = TopGamesPresenterProtocolMock()
        let userPresenterUnderTest = TopGamesPresenter(service: twoGamesApiMock)
        userPresenterUnderTest.attachView(view: viewMock)
        
        //when
        userPresenterUnderTest.loadData()
        
        //verify
        XCTAssertTrue(viewMock.setStartLoadingCalled)
        XCTAssertTrue(viewMock.setFinishLoadingCalled)
    }
    
    func testAttachAndDetachView() {
        //given
        let viewMock = TopGamesPresenterProtocolMock()
        let userPresenterUnderTest = TopGamesPresenter(service: twoGamesApiMock)
        
        //when
        userPresenterUnderTest.attachView(view: viewMock)
        
        //verify
        XCTAssertNotNil(userPresenterUnderTest.topGameProtocol)
        userPresenterUnderTest.detachView()
        XCTAssertNil(userPresenterUnderTest.topGameProtocol)
    }
    
    func testHasMoreData() {
        let viewMock = TopGamesPresenterProtocolMock()
        let userPresenterUnderTest = TopGamesPresenter(service: paginationApiMock)
        userPresenterUnderTest.attachView(view: viewMock)
        userPresenterUnderTest.total = 4
        userPresenterUnderTest.data = [TopModel(), TopModel()]
        XCTAssertTrue(userPresenterUnderTest.hasMoreData())
    }
    
    func testNoHasMoreData() {
        let viewMock = TopGamesPresenterProtocolMock()
        let userPresenterUnderTest = TopGamesPresenter(service: twoGamesApiMock)
        userPresenterUnderTest.attachView(view: viewMock)
        XCTAssertFalse(userPresenterUnderTest.hasMoreData())
    }
    
    func testFilterGamesWithName() {
        //given
        let viewMock = TopGamesPresenterProtocolMock()
        let userPresenterUnderTest = TopGamesPresenter(service: twoGamesApiMock)
        userPresenterUnderTest.attachView(view: viewMock)
        
        let topModel1 = TopModel()
        topModel1.game.name = "abc"
        
        let topModel2 = TopModel()
        topModel2.game.name = "def"
        
        userPresenterUnderTest.data = [topModel1, topModel2]
        userPresenterUnderTest.completeData = [topModel1, topModel2]
        
        //When
        userPresenterUnderTest.filterGamesWithName(searchText: "a")
        
        //verify
        XCTAssert(userPresenterUnderTest.data.count == 1)
        XCTAssertEqual(userPresenterUnderTest.data.first?.game.name, "abc")
        XCTAssertTrue(viewMock.setTopGamesCalled)
    }
    
    func testAddMoreGames() {
        
        let viewMock = TopGamesPresenterProtocolMock()
        let userPresenterUnderTest = TopGamesPresenter(service: twoGamesApiMock)
        userPresenterUnderTest.attachView(view: viewMock)
        
        userPresenterUnderTest.total = 4
        
        userPresenterUnderTest.addMoreGames(yOffset: 30, height: 90)
        XCTAssertTrue(viewMock.setFinishLoadingCalled)
        
        userPresenterUnderTest.total = 4
        viewMock.setFinishLoadingCalled = false
        userPresenterUnderTest.addMoreGames(yOffset: 50, height: 90)
        XCTAssertTrue(viewMock.setFinishLoadingCalled)
        
        userPresenterUnderTest.total = 4
        viewMock.setFinishLoadingCalled = false
        userPresenterUnderTest.addMoreGames(yOffset: 10, height: 90)
        XCTAssertFalse(viewMock.setFinishLoadingCalled)
        
    }
}


