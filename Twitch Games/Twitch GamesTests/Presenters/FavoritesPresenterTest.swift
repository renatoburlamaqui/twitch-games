import XCTest
@testable import Twitch_Games

class FavoritePresenterProtocolMock : NSObject, FavoritePresenterProtocol {
    var setSFavoritesCalled = false
    var setRemoveFavoriteItemCalled = false
    var setAddFavoriteItemCalled = false
    var setEmptyStateCalled = false
    var setErrorStateCalled = false
    
    func setFavorites(favorites: [TopModel]){
        setSFavoritesCalled = true
    }
    
    func setEmptyState() {
        setEmptyStateCalled = true
    }
    
    func setErrorState() {
        setErrorStateCalled = true
    }
    
    func removeFavoriteItem(items: [TopModel]) {
        setRemoveFavoriteItemCalled = true
    }
    
    func addFavoriteItem(items: [TopModel]) {
        setAddFavoriteItemCalled = true
    }
}

class FavoritesPresenterTest: XCTestCase {
    
    let emptyApiMock = ApiMock(response: HomeResponse(total: 0, top: []))
    let twoGamesApiMock = ApiMock(response: HomeResponse(total: 2, top: [TopModel(), TopModel()]))
    let paginationApiMock = ApiMock(response: HomeResponse(total: 4, top: [TopModel(), TopModel()]))
    
    
    func testShouldSetFavoritStateCalled() {
        //given
        let viewMock = FavoritePresenterProtocolMock()
        let userPresenterUnderTest = FavoritesPresenter()
        let dataHelper = DataHelperMock()
        userPresenterUnderTest.attachView(view: viewMock)
        FavoritesModel.shared.clear()
        
        //when
        userPresenterUnderTest.loadFavorites(dataHelper: dataHelper)
        
        //verify
        XCTAssertTrue(viewMock.setEmptyStateCalled)
    }
    
    func testShouldSetGamesCalled() {
        //given
        let viewMock = FavoritePresenterProtocolMock()
        let userPresenterUnderTest = FavoritesPresenter()
        let dataHelper = DataHelperMock()
        userPresenterUnderTest.attachView(view: viewMock)
        FavoritesModel.shared.clear()
        FavoritesModel.shared.addItemInFavorites(TopModel())
        
        //when
        userPresenterUnderTest.loadFavorites(dataHelper: dataHelper)

        //verify
        XCTAssertTrue(viewMock.setSFavoritesCalled)
    }

    func testShouldAddFavoriteItemCalled() {
        //given
        let viewMock = FavoritePresenterProtocolMock()
        let userPresenterUnderTest = FavoritesPresenter()
        let dataHelper = DataHelperMock()
        userPresenterUnderTest.attachView(view: viewMock)
        FavoritesModel.shared.clear()

        //when
        userPresenterUnderTest.addFavorite(dataHelper: dataHelper, TopModel())

        //verify
        XCTAssertTrue(viewMock.setAddFavoriteItemCalled)
    }
    
    func testShouldRemoveavoriteItemCalled() {
        //given
        let viewMock = FavoritePresenterProtocolMock()
        let userPresenterUnderTest = FavoritesPresenter()
        let dataHelper = DataHelperMock()
        userPresenterUnderTest.attachView(view: viewMock)
        
        let topModel1 = TopModel()
        topModel1.game.name = "abc"
        
        let topModel2 = TopModel()
        topModel2.game.name = "def"
        
        FavoritesModel.shared.clear()
        FavoritesModel.shared.addItemInFavorites(topModel1)
        FavoritesModel.shared.addItemInFavorites(topModel2)
        
        //when
        userPresenterUnderTest.removeFavorite(dataHelper: dataHelper, topModel1)
        
        //verify
        XCTAssertTrue(viewMock.setRemoveFavoriteItemCalled)
    }
    
    func testAttachAndDetachView() {
        //given
        let viewMock = FavoritePresenterProtocolMock()
        let userPresenterUnderTest = FavoritesPresenter()

        //when
        userPresenterUnderTest.attachView(view: viewMock)

        //verify
        XCTAssertNotNil(userPresenterUnderTest.favoritesProtocol)
        userPresenterUnderTest.detachView()
        XCTAssertNil(userPresenterUnderTest.favoritesProtocol)
    }
}


