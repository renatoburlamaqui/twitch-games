import XCTest
@testable import Twitch_Games

class ApiMock: Api {
    private let response: HomeResponse
    init(response: HomeResponse) {
        self.response = response
    }
    
    override func getTopGames(offset: Int, completion: @escaping (Result<HomeResponse>) -> ()) {
        completion(.success(response))
    }
}
