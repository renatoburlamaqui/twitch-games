import XCTest
@testable import Twitch_Games

class GameModelMock : GameModel {
    func createMock(name: String = "", popularity: Int = 100, _id: Int = 100, logo: LogoModel = LogoModel(), box: LogoModel = LogoModel()) {
        self.name = name
        self.popularity = popularity
        self._id = _id
        self.logo = logo
        self.box = box
    }
}

