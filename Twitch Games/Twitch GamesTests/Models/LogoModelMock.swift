import XCTest
@testable import Twitch_Games

class LogoModelMock : LogoModel {
    func createMock(large : String = "", medium : String = "", small : String = "", template : String = "")  {
        self.large = large
        self.medium = medium
        self.small = small
        self.template = template
    }
}
