import XCTest
@testable import Twitch_Games


class TopModelMock : TopModel {
    func createMock(game:GameModel = GameModel(), viewers:Int = 100, channels:Int = 100) {
        self.game = game
        self.viewers = viewers
        self.channels = channels
    }
}
