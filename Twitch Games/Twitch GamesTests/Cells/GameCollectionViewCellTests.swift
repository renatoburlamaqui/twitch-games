import XCTest
@testable import Twitch_Games


class GameCollectionViewCellTests : XCTestCase {
    func testGameCollectionViewCellContainsAView() {
        let bundle = Bundle(for: GameCollectionViewCell.self)
        
        guard let _ = bundle.loadNibNamed("GameCollectionViewCell", owner: nil)?.first as? UIView else {
            return XCTFail("GameCollectionViewCell nib did not contain a UIView")
        }
    }
}
