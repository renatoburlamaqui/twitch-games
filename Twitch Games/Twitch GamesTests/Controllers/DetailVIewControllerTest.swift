import XCTest
@testable import Twitch_Games

class DetailVIewControllerTest: XCTestCase {
    
    var controller: DetailViewController!
    
    override func setUp() {
        super.setUp()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        controller = storyboard.instantiateViewController(withIdentifier:"DetailViewController") as! DetailViewController
        
        //Load view components
        let _ = controller.view
    }
    
    func testOutlets() {
        XCTAssertNotNil(controller.icon)
        XCTAssertNotNil(controller.lblTitle)
        XCTAssertNotNil(controller.lblvisualizations)
        XCTAssertNotNil(controller.favoriteButton)
    }
    
    func testToogleFavorite() {
        self.controller.favoriteButton.isSelected = true
        self.controller.toogleFavorite()
        XCTAssertFalse(self.controller.favoriteButton.isSelected)
        self.controller.toogleFavorite()
        XCTAssertTrue(self.controller.favoriteButton.isSelected)
    }
    
    func testSetupInfos() {
        let topTest = TopModel()
        topTest.game.name = "Teste name"
        topTest.viewers = 1000
        self.controller.top = topTest
        
        self.controller.setupInfos()
        
        XCTAssertEqual(self.controller.lblTitle.text, topTest.game.name)
        XCTAssertEqual(self.controller.lblvisualizations.text, "Visualizations: \(topTest.viewers)")
    }
    
}
