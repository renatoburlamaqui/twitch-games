@testable import Twitch_Games
import XCTest

class TopGamesViewControllerTest: XCTestCase {
    
     var controller: TopGamesViewController!
    
    override func setUp() {
        super.setUp()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        controller = storyboard.instantiateViewController(withIdentifier:"TopGamesViewController") as! TopGamesViewController
    
        //Load view components
         let _ = controller.view
    }
    
    func testOutlets() {
        XCTAssertNotNil(controller.collectionView)
        XCTAssertNotNil(controller.searchBar)
        XCTAssertNotNil(controller.errorMessage)
    }
    
    func testCallDelegatesAndDataSources() {
        XCTAssertNotNil(controller.collectionView.delegate)
        XCTAssertNotNil(controller.collectionView.dataSource)
        XCTAssertNotNil(controller.collectionView.dragDelegate)
        XCTAssertNotNil(controller.collectionView.dropDelegate)
    }
    
    func testPresenterIsAtached() {
        XCTAssertNotNil(controller.topGamesPresenter)
    }
    
    func testSetTopGames() {
        controller.setTopGames(topGames: [TopModel(), TopModel(), TopModel()])
        XCTAssert(controller.dataToDisplay.count == 3)
        XCTAssertFalse(controller.searchBar.isHidden)
        XCTAssertFalse(controller.collectionView.isHidden)
    }
    
    func testSetEmptyState() {
        controller.setEmptyState()
        XCTAssertEqual(controller.errorMessage.text, "No games to be displayed")
        XCTAssertTrue(controller.searchBar.isHidden)
        XCTAssertTrue(controller.collectionView.isHidden)
    }
    
    func testSetErrorState() {
        let errorMessage = "Error message"
        controller.setErrorState(errorMessage: errorMessage)
        XCTAssertEqual(controller.errorMessage.text, errorMessage)
        XCTAssertTrue(controller.searchBar.isHidden)
        XCTAssertTrue(controller.collectionView.isHidden)
    }
    
    func testAddPullToRefresh() {
        controller.addPullToRefresh()
        
        for subview in controller.collectionView.subviews {
            if let _ = subview as? UIRefreshControl {
                XCTAssert(true)
            }
        }
    }
    
}
