import XCTest
@testable import Twitch_Games

class FavoritesViewControllerTest: XCTestCase {
    
    var controller: FavoritesViewController!
    
    override func setUp() {
        super.setUp()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        controller = storyboard.instantiateViewController(withIdentifier:"FavoritesViewController") as! FavoritesViewController
        
        //Load view components
        let _ = controller.view
    }
    
    func testOutlets() {
        XCTAssertNotNil(controller.collectionView)
        XCTAssertNotNil(controller.errorMessage)
    }
    
    func testCallDelegatesAndDataSources() {
        XCTAssertNotNil(controller.collectionView.delegate)
        XCTAssertNotNil(controller.collectionView.dataSource)
        XCTAssertNotNil(controller.collectionView.dragDelegate)
        XCTAssertNotNil(controller.collectionView.dropDelegate)
    }
    
    func testPresenterIsAtached() {
        XCTAssertNotNil(controller.favoritePresenter)
    }
    
    func testSetFavorites() {
        controller.setFavorites(favorites: [TopModel(), TopModel(), TopModel()])
        XCTAssert(controller.dataToDisplay.count == 3)
        XCTAssertFalse(controller.collectionView.isHidden)
    }
    
    
    func testSetEmptyState() {
        controller.setEmptyState()
        XCTAssertEqual(controller.dataToDisplay.count, 0)
        XCTAssertTrue(controller.collectionView.isHidden)
    }
    
    func testRemoveFavorite() {
        controller.removeFavoriteItem(items: [TopModel(), TopModel(), TopModel()])
        XCTAssertEqual(controller.dataToDisplay.count, 3)
    }
    
    func testAddFavorite() {
        controller.addFavoriteItem(items: [TopModel(), TopModel(), TopModel()])
        XCTAssertEqual(controller.dataToDisplay.count, 3)
    }
    
    func testErrorState() {
        controller.setErrorState()
        XCTAssertTrue(controller.collectionView.isHidden)
    }
}
