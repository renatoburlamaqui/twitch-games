import XCTest
@testable import Twitch_Games

class DataHelperMock: DataHelper {
    override func fetchFavorites() -> [TopModel] {
        return FavoritesModel.shared.top
    }
    
    override func saveGameOnFavorites(top: TopModel) {}
    
    override func removeGameOnfavorites(top: TopModel) {}
}
