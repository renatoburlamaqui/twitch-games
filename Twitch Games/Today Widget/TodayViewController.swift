//
//  TodayViewController.swift
//  Today Widget
//
//  Created by Renato Medina on 06/03/2018.
//  Copyright © 2018 Renato Medina. All rights reserved.
//

import UIKit
import NotificationCenter

class TodayViewController: UIViewController, NCWidgetProviding, UITableViewDelegate, UITableViewDataSource {
    
    private let api = Api()
    private var data: [TopModel] = []
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOSApplicationExtension 10.0, *) {
            extensionContext?.widgetLargestAvailableDisplayMode = .expanded
        }
        self.preferredContentSize.height = 300
        
        self.loadData()
    }
    
    // MARK: - NCWidgetProviding
    
    func widgetPerformUpdate(completionHandler: (@escaping (NCUpdateResult) -> Void)) {
        loadData()
        completionHandler(NCUpdateResult.newData)
    }
    
    func widgetMarginInsetsForProposedMarginInsets(defaultMarginInsets: UIEdgeInsets) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    @available(iOSApplicationExtension 10.0, *)
    func widgetActiveDisplayModeDidChange(_ activeDisplayMode: NCWidgetDisplayMode, withMaximumSize maxSize: CGSize) {
        if activeDisplayMode == .expanded {
            preferredContentSize = CGSize(width: maxSize.width, height: 400)
        }
        else if activeDisplayMode == .compact {
            preferredContentSize = maxSize
        }
    }
    
    // MARK: - Loading of data
    
    func loadData() {
        api.getTopGames(offset: 0) { (result) in
            self.data = []
            switch result {
            case .success(let response):
                let topSorted = response.top.sorted {
                    $0.game.popularity < $1.game.popularity
                }
                
                let maxIndex = topSorted.count >= 3 ? 3 : topSorted.count
                
                for i in 0..<maxIndex {
                    self.data.append(topSorted[i])
                }
                self.tableView.reloadData()
            case .failure(let errorMessage):
                debugPrint(errorMessage)
            }
        }
    }
    
    // MARK: - TableView Data Source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "widgettableViewCellIdentifier", for: indexPath)
        
        let item = data[indexPath.row]
        cell.textLabel?.text = item.game.name
        cell.textLabel?.textColor = UIColor.white
        
        return cell
    }
}
