
import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        ApperanceProxyHelper.customizeNavigationBar()
        ApperanceProxyHelper.customizeBackButton()
        
        return true
    }

    func applicationWillTerminate(_ application: UIApplication) {
        DataHelper().saveContext()
    }
}

