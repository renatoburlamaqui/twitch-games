
import UIKit
import Networking

class Api {
    
    let defaultHeaders = [
        "Accept": "application/vnd.twitchtv.v5+json",
        "Client-ID": "ew9btk4tggfe9cq4gbmckor3rb3ujx"
    ]
    
    private lazy var networking: Networking = {
        Networking(baseURL: "https://api.twitch.tv/kraken/games")
    }()
    
    init() {
        self.networking.headerFields = defaultHeaders
    }
    
    func getTopGames(offset:Int = 0, completion: @escaping (_ result: Result<HomeResponse>) -> ()) {
        self.networking.get("/top?limit=20&offset=\(offset)") { result in
            switch result {
            case .success(let response):

                let decoder = JSONDecoder()

                do {
                    let response = try decoder.decode(HomeResponse.self, from: response.data)
                    completion(.success(response))
                } catch {
                    completion(.failure("Return api error"))
                }
            case .failure(let response):
                var messageError = ""
                switch response.statusCode {
                case 400...499:
                    messageError = "Url not found"
                case 500...599:
                    messageError = "Error: Server not found"
                default:
                    messageError = response.error.localizedDescription
                }
            
                completion(.failure(messageError))
            }
        }
    }
}

enum Result<Value> {
    case success(Value)
    case failure(String)
}
