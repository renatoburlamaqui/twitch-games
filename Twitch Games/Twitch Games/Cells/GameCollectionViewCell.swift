

import UIKit

protocol  GameCollectionViewCellDelegate {
    func favoriteGame(item:TopModel)
    func unfavoriteGame(item:TopModel)
}

class GameCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var icon: UIImageView!
    @IBOutlet var title: UILabel!
    @IBOutlet var favButton: UIButton!
    
    var gameDelegate: GameCollectionViewCellDelegate?
    
    var topItem: TopModel?
    
    class func cellNib() -> UINib {
        return UINib(nibName: "GameCollectionViewCell", bundle: nil)
    }
    
    class func cellIdentifier() -> String {
        return "homeCollectionCellIdentifier"
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func configureCell(item:TopModel, hasFavorited:Bool = true) {
        self.topItem = item
        self.favButton.isSelected = item.hasFavoritted()
        self.title.text = item.game.name
        self.icon.download(image: item.game.box.medium)
    }
    
    @IBAction func toogleFavorite(_ sender: Any) {
        self.favButton.isSelected ? self.gameDelegate?.unfavoriteGame(item: self.topItem!) : self.gameDelegate?.favoriteGame(item: self.topItem!)
        self.favButton.isSelected = !self.favButton.isSelected
    }
}
