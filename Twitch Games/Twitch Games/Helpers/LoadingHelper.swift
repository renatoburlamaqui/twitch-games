//
//  LoadingHelper.swift
//  Twitch Games
//
//  Created by Patrícia Pampanelli on 05/03/2018.
//  Copyright © 2018 Renato Medina. All rights reserved.
//

import UIKit
import MaterialComponents

import Foundation

class LoadingHelper {
    static let shared = LoadingHelper()
    
    private let loadingView = UIView(frame: UIScreen.main.bounds)
    private let activityIndicator = MDCActivityIndicator(frame: UIScreen.main.bounds)

    
    private func createLoadingView() {
        loadingView.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.6)
        
        activityIndicator.radius = 24
        activityIndicator.strokeWidth = 4
        
        activityIndicator.cycleColors = [UIColor(hexString: "#00AEEF")]
        loadingView.addSubview(activityIndicator)
        loadingView.bringSubview(toFront: activityIndicator)
        
        activityIndicator.startAnimating()
    }
    
    func showStatusLoading() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func hideStatusLoading() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    func showActivityIndicator() {
        showStatusLoading()
        createLoadingView()
        UIApplication.shared.keyWindow?.addSubview(loadingView)
    }
    
    func hideActivityIndicator(animated: Bool = true){
        hideStatusLoading()
        activityIndicator.stopAnimating()
        activityIndicator.removeFromSuperview()
        loadingView.removeFromSuperview()
    }
}
