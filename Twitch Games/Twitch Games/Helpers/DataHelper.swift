//
//  DataHelper.swift
//  Twitch Games
//
//  Created by Patrícia Pampanelli on 04/03/2018.
//  Copyright © 2018 Renato Medina. All rights reserved.
//

import UIKit
import CoreData

class DataHelper: NSObject {
    override init() {
        super.init()
    }
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "Twitch_Games")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    func removeGameOnfavorites(top:TopModel) {
        let dataHelper = DataHelper()
        let managedContext = dataHelper.persistentContainer.viewContext
        let fetchRequest: NSFetchRequest<Favorites> = Favorites.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "id==\(top.game._id)")
        
        do {
            if let object  = try managedContext.fetch(fetchRequest).first {
                managedContext.delete(object)
            }
            try managedContext.save() 
        } catch {
            debugPrint("Error in removeGameOnFavorites")
        }
    }
    
    func saveGameOnFavorites(top:TopModel) {
        let dataHelper = DataHelper()
        let managedContext = dataHelper.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "Favorites", in: managedContext)!
        let topGames = Favorites(entity: entity, insertInto: managedContext)
        
        topGames.setValue(top.game._id, forKey: "id")
        topGames.setValue(top.game.name, forKey: "name")
        topGames.setValue(top.game.popularity, forKey: "popularity")
        topGames.setValue(top.viewers, forKey: "viewers")
        topGames.setValue(top.game.box.medium, forKey: "logoSmallUrl")
        topGames.setValue(top.game.box.large, forKey: "logolargeUrl")
        
        do {
            try managedContext.save()
            
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    func fetchFavorites() -> [TopModel] {
        
        FavoritesModel.shared.clear()
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Favorites")
        let context = DataHelper().persistentContainer.viewContext
        do{
            let results = try context.fetch(fetchRequest) as! [Favorites]
            for result in results {
                let favoriteResult = self.createTopModelWithFavorite(favorite: result)
                FavoritesModel.shared.top.append(favoriteResult)
            }
            return FavoritesModel.shared.top;
        }catch{
            debugPrint("Error load Favorites list")
            return []
        }
    }
    
    func createTopModelWithFavorite(favorite: Favorites) -> TopModel {
        let topModel = TopModel()
        topModel.game._id = Int(favorite.id)
        topModel.game.name = favorite.name ?? ""
        topModel.game.popularity = Int(favorite.popularity)
        topModel.viewers = Int(favorite.viewers)
        topModel.game.box.medium = favorite.logoSmallUrl ?? ""
        topModel.game.box.large = favorite.logolargeUrl ?? ""
        return topModel
    }

}
