
import UIKit
import CoreData

protocol FavoritePresenterProtocol: NSObjectProtocol {
    func setFavorites(favorites: [TopModel])
    func setEmptyState()
    func setErrorState()
    func removeFavoriteItem(items: [TopModel])
    func addFavoriteItem(items: [TopModel])
}

class FavoritesPresenter {
    
    weak var favoritesProtocol : FavoritePresenterProtocol?
    
    
    func attachView(view:FavoritePresenterProtocol){
        self.favoritesProtocol = view
    }
    
    func detachView() {
        self.favoritesProtocol = nil
    }
    
    func loadFavorites(dataHelper: DataHelper = DataHelper()) {
        let _ = dataHelper.fetchFavorites()
        FavoritesModel.shared.isEmpty() ? favoritesProtocol?.setEmptyState() : favoritesProtocol?.setFavorites(favorites: FavoritesModel.shared.top)
    }
    
    func addFavorite(dataHelper: DataHelper = DataHelper(), _ item:TopModel) {
        FavoritesModel.shared.addItemInFavorites(item)
        dataHelper.saveGameOnFavorites(top: item)
        self.favoritesProtocol?.addFavoriteItem(items: FavoritesModel.shared.top)
    }
    
    func removeFavorite(dataHelper: DataHelper = DataHelper(), _ item:TopModel) {
        FavoritesModel.shared.removeItemInFavorites(item)
        dataHelper.removeGameOnfavorites(top: item)
        FavoritesModel.shared.isEmpty() ? favoritesProtocol?.setEmptyState() : favoritesProtocol?.removeFavoriteItem(items: FavoritesModel.shared.top)
    }

}
