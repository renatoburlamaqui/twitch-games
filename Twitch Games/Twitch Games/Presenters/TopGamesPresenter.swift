
import UIKit

protocol TopGamesPresenterProtocol: NSObjectProtocol {
    func startLoading()
    func finishLoading()
    func setTopGames(topGames: [TopModel])
    func setEmptyState()
    func setErrorState(errorMessage: String)
}

class TopGamesPresenter {
    private let service: Api
    weak var topGameProtocol : TopGamesPresenterProtocol?
    
    var isPaginating = false
    var total = 0
    var data: [TopModel] = []
    var completeData: [TopModel] = []
    
    init(service:Api){
        self.service = service
    }
    
    func attachView(view:TopGamesPresenterProtocol){
        self.topGameProtocol = view
    }
    
    func detachView() {
        self.topGameProtocol = nil
    }
    
    func hasMoreData() -> Bool {
        return (self.total > self.data.count) && !isPaginating
    }
    
    func filterGamesWithName(searchText:String) {
        if searchText.isEmpty {
            self.data = self.completeData
        }else{
            self.data = completeData.filter({ (topModel) -> Bool in
                return topModel.game.name.lowercased().contains(searchText.lowercased())
            })
        }
        self.topGameProtocol?.setTopGames(topGames: self.data)
    }
    
    func addMoreGames(yOffset:CGFloat, height:CGFloat) {
        if (yOffset >= height/3) && self.hasMoreData() {
            self.loadData()
        }
    }
    
    func loadData() {
        
        self.isPaginating = true
        let offset = self.data.count
        
        if total == 0 {
            self.topGameProtocol?.startLoading()
        }
        
        self.service.getTopGames(offset: offset) { (result) in
            switch result {
            case .success(let response):
                if (response._total == 0) {
                    self.topGameProtocol?.setEmptyState()
                }else{
                    self.total = response._total
                    self.completeData.append(contentsOf: response.top)
                    self.data = self.completeData
                    self.topGameProtocol?.setTopGames(topGames: self.data)
                }
            case .failure(let errorMessage):
                  self.topGameProtocol?.setErrorState(errorMessage: errorMessage)
            }
            self.isPaginating = false
            self.topGameProtocol?.finishLoading()
        }
    }
    
}
