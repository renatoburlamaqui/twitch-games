//
//  SearchTopGamesCollectionReusableView.swift
//  Twitch Games
//
//  Created by Renato Medina on 02/03/2018.
//  Copyright © 2018 Renato Medina. All rights reserved.
//

import UIKit

class SearchTopGamesCollectionReusableView: UICollectionReusableView {
        
    @IBOutlet weak var searchBar: UISearchBar!
}
