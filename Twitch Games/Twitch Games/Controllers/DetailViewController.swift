

import UIKit

class DetailViewController: UIViewController {

    //Outlets
    @IBOutlet var favoriteButton: UIButton!
    @IBOutlet var icon: UIImageView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblvisualizations: UILabel!
    
    //Variables
    var top = TopModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.topItem?.title = ""
        self.title = top.game.name
        
        self.setupInfos()
        
    }
    
    @IBAction func toogleFavorite() {
        self.favoriteButton.isSelected ? self.unfavoriteGame() : self.favoriteGame()
        self.favoriteButton.isSelected = !self.favoriteButton.isSelected
    }
}

extension DetailViewController {
    func setupInfos() {
        icon.download(image: top.game.box.large)
        lblTitle.text = top.game.name
        lblvisualizations.text = "Visualizations: \(top.viewers)"
        favoriteButton.isSelected = top.hasFavoritted()
    }
    
    func favoriteGame() {
        FavoritesModel.shared.addItemInFavorites(top)
        DataHelper().saveGameOnFavorites(top: top)
        
    }
    
    func unfavoriteGame() {
        FavoritesModel.shared.removeItemInFavorites(top)
        DataHelper().removeGameOnfavorites(top: top)
    }
}
