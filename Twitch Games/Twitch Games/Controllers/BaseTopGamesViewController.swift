//
//  BaseTopGamesViewController.swift
//  Twitch Games
//
//  Created by Patrícia Pampanelli on 01/03/2018.
//  Copyright © 2018 Renato Medina. All rights reserved.
//

import UIKit
import CoreData

class BaseTopGamesViewController: UIViewController {
    
    //Variables
    var dataToDisplay: [TopModel] = []
    var favoritePresenter : FavoritesPresenter?
    
    //Outlets
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var errorMessage: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView.dragDelegate = self
        self.collectionView.dragInteractionEnabled = true
        self.collectionView.dropDelegate = self
        self.collectionView.register(GameCollectionViewCell.cellNib(), forCellWithReuseIdentifier: GameCollectionViewCell.cellIdentifier())
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.loadFavorites()
    }
    
    func removeItemInFavorites(_ item:TopModel, index: IndexPath) {
        FavoritesModel.shared.removeItemInFavorites(item)
        DataHelper().removeGameOnfavorites(top: item)
    }
    
    func loadFavorites() {
        let _ = DataHelper().fetchFavorites()
        self.collectionView.reloadData()
    }
}

// MARK - GameCollectionViewCellDelegate methods
extension BaseTopGamesViewController: GameCollectionViewCellDelegate {
    
    func favoriteGame(item:TopModel) {
        if let favoritePresenter  = self.favoritePresenter {
            favoritePresenter.addFavorite(item)
        }else{
            FavoritesModel.shared.addItemInFavorites(item)
            DataHelper().saveGameOnFavorites(top: item)
        }
    }
    
    func unfavoriteGame(item:TopModel) {
        if let favoritePresenter  = self.favoritePresenter {
            favoritePresenter.removeFavorite(item)
        } else if let indexToObject = FavoritesModel.shared.top.index(where: { $0.game.name == item.game.name }) {
            self.removeItemInFavorites(item, index: IndexPath(index: indexToObject))
        }
    }
}

// MARK - UICollectionViewDataSource methods
extension BaseTopGamesViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataToDisplay.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let top = dataToDisplay[indexPath.row]
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: GameCollectionViewCell.cellIdentifier(), for: indexPath) as! GameCollectionViewCell
        cell.gameDelegate = self
        cell.configureCell(item: top)
        
        return cell
    }
}

// MARK - UICollectionViewDragDelegate methods
extension BaseTopGamesViewController: UICollectionViewDragDelegate {
    func collectionView(_ collectionView: UICollectionView, itemsForBeginning session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem] {
        let item = dataToDisplay[indexPath.row]
        let itemProvider = NSItemProvider(object: item.game.name as NSString)
        let dragItem = UIDragItem(itemProvider: itemProvider)
        dragItem.localObject = item
        return [dragItem]
    }
    
    func collectionView(_ collectionView: UICollectionView, dragSessionWillBegin session: UIDragSession) {
        self.tabBarController?.selectedIndex = 1
        
        if let favoriteVC = self.tabBarController?.viewControllers?[1] as? FavoritesViewController {
            favoriteVC.collectionView.isHidden = false
        }
    }
}

// MARK - UICollectionViewDropDelegate methods
extension BaseTopGamesViewController: UICollectionViewDropDelegate {
    private func reorderItems(coordinator: UICollectionViewDropCoordinator, destinationIndexPath: IndexPath, collectionView: UICollectionView)
    {
        let items = coordinator.items
        if items.count == 1, let item = items.first, let sourceIndexPath = item.sourceIndexPath
        {
            var dIndexPath = destinationIndexPath
            if dIndexPath.row >= collectionView.numberOfItems(inSection: 0)
            {
                dIndexPath.row = collectionView.numberOfItems(inSection: 0) - 1
            }
            collectionView.performBatchUpdates({
                self.dataToDisplay.remove(at: sourceIndexPath.row)
                self.dataToDisplay.insert(item.dragItem.localObject as! TopModel, at: dIndexPath.row)
                collectionView.deleteItems(at: [sourceIndexPath])
                collectionView.insertItems(at: [dIndexPath])
            })
            coordinator.drop(items.first!.dragItem, toItemAt: dIndexPath)
        }
    }
    
    private func copyItems(coordinator: UICollectionViewDropCoordinator, destinationIndexPath: IndexPath, collectionView: UICollectionView)
    {
        collectionView.performBatchUpdates({
            var indexPaths = [IndexPath]()
            for (index, item) in coordinator.items.enumerated()
            {
                let indexPath = IndexPath(row: destinationIndexPath.row + index, section: destinationIndexPath.section)
                let item = item.dragItem.localObject as! TopModel
                
                if !item.hasFavoritted() {
                    self.favoritePresenter?.addFavorite(item)
                    indexPaths.append(indexPath)
                }
            }
            collectionView.insertItems(at: indexPaths)
        })
    }
    
    
    func collectionView(_ collectionView: UICollectionView, performDropWith coordinator: UICollectionViewDropCoordinator) {
        var destinationIndexPath: IndexPath
        if let indexPath = coordinator.destinationIndexPath
        {
            destinationIndexPath = indexPath
        }
        else
        {
            // Get last index path of collection view.
            let section = collectionView.numberOfSections - 1
            let row = collectionView.numberOfItems(inSection: section)
            destinationIndexPath = IndexPath(row: row, section: section)
        }
        
        switch coordinator.proposal.operation
        {
        case .move:
            self.reorderItems(coordinator: coordinator, destinationIndexPath:destinationIndexPath, collectionView: collectionView)
            break
        case .copy:
            self.copyItems(coordinator: coordinator, destinationIndexPath: destinationIndexPath, collectionView: collectionView)
            break
        default:
            return
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, dropSessionDidUpdate session: UIDropSession, withDestinationIndexPath destinationIndexPath: IndexPath?) -> UICollectionViewDropProposal
    {
        if session.localDragSession != nil
        {
            if collectionView.hasActiveDrag
            {
                return UICollectionViewDropProposal(operation: .move, intent: .insertAtDestinationIndexPath)
            }
            else
            {
                return UICollectionViewDropProposal(operation: .copy, intent: .insertAtDestinationIndexPath)
            }
        }
        else
        {
            return UICollectionViewDropProposal(operation: .forbidden)
        }
    }
}


