

import UIKit
import CoreData

class FavoritesViewController: BaseTopGamesViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        favoritePresenter = FavoritesPresenter()
        favoritePresenter?.attachView(view: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.topItem?.title = "Favorites"
        self.favoritePresenter?.loadFavorites()
    }
}


// MARK - FavoritePresenterProtocol methods
extension FavoritesViewController: FavoritePresenterProtocol {
    func setFavorites(favorites: [TopModel]) {
        self.collectionView.isHidden = false
        self.dataToDisplay = favorites
        self.collectionView.reloadData()
    }
    
    func setEmptyState() {
        self.dataToDisplay = []
        self.collectionView.isHidden = true
    }
    
    func removeFavoriteItem(items: [TopModel]) {
        self.dataToDisplay = items
        self.collectionView.reloadData()
    }
    
    func addFavoriteItem(items: [TopModel]) {
        self.dataToDisplay = items
        self.collectionView.reloadData()
    }
    
    func setErrorState() {
        self.collectionView.isHidden = true
    }
}

// MARK - FavoritesViewController methods
extension FavoritesViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "favoriteTodetailViewControllerSegue", sender: FavoritesModel.shared.top[indexPath.row])
    }
}

// MARK - navigation methods
extension FavoritesViewController {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "favoriteTodetailViewControllerSegue" {
            if let destinationVC = segue.destination as? DetailViewController  {
                let topModel = sender as! TopModel
                destinationVC.top = topModel
            }
        }
    }
}

