

import UIKit

class TopGamesViewController: BaseTopGamesViewController {
    
    //Outlets
    @IBOutlet var searchBar: UISearchBar!
    
    
    //Constants
    private let api = Api()
    private let refreshControl = UIRefreshControl()
    let topGamesPresenter = TopGamesPresenter(service: Api())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        topGamesPresenter.attachView(view: self)
        topGamesPresenter.loadData()
        self.addPullToRefresh()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.topItem?.title = "Top Games"
    }
}

//MARK - Proviate Methods
extension TopGamesViewController {
    @objc private func refresh() {
        self.topGamesPresenter.loadData()
    }
    
    func addPullToRefresh() {
        
        if #available(iOS 10.0, *) {
            self.collectionView.refreshControl = refreshControl
        } else {
            self.collectionView.addSubview(refreshControl)
        }
        
        self.refreshControl.attributedTitle = NSAttributedString(string: "Updating...")
        self.refreshControl.addTarget(self, action: #selector(TopGamesViewController.refresh), for: .valueChanged)
        self.refreshControl.tintColor = UIColor(red:0.25, green:0.72, blue:0.85, alpha:1.0)
    }
}

//MARK - TopGamesPresenterProtocol methods
extension TopGamesViewController: TopGamesPresenterProtocol {
    func startLoading() {
        LoadingHelper.shared.showActivityIndicator()
    }
    
    func finishLoading() {
        LoadingHelper.shared.hideActivityIndicator()
    }
    
    func setTopGames(topGames: [TopModel]) {
        self.dataToDisplay = topGames
        self.searchBar.isHidden = false
        self.collectionView.isHidden = false
        self.collectionView.reloadData()
        self.refreshControl.endRefreshing()
    }
    
    func setEmptyState() {
        self.collectionView.isHidden = true
        self.searchBar.isHidden = true
        self.errorMessage.text = "No games to be displayed"
    }
    
    func setErrorState(errorMessage: String) {
        self.collectionView.isHidden = true
        self.searchBar.isHidden = true
        self.errorMessage.text = errorMessage
    }
}

//MARK - UICollectionViewDelegate methods
extension TopGamesViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "topTodetailViewControllerSegue", sender: dataToDisplay[indexPath.row])
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        if (kind == UICollectionElementKindSectionHeader) {
            let headerView:UICollectionReusableView =  collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "topGamesCollectionViewHeaderIdentifier", for: indexPath)
            
            return headerView
        }
        
        return UICollectionReusableView()
        
    }
}

//MARK - UISearchBarDelegate methods
extension TopGamesViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.topGamesPresenter.filterGamesWithName(searchText: searchBar.text ?? "")
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.topGamesPresenter.filterGamesWithName(searchText: searchBar.text ?? "")
    }
}

//MARK - UIScrollViewDelegate methods
extension TopGamesViewController: UIScrollViewDelegate {
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.topGamesPresenter.addMoreGames(yOffset: scrollView.contentOffset.y, height: scrollView.contentSize.height)
    }
}

extension TopGamesViewController {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "topTodetailViewControllerSegue" {
            if let destinationVC = segue.destination as? DetailViewController  {
                if let topModel = sender as? TopModel {
                    destinationVC.top = topModel
                }
            }
        }
    }
}
