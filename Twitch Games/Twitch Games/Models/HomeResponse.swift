
import Foundation
import UIKit

class HomeResponse : Codable {
    var _total : Int
    var top : [TopModel]
    
    init(total:Int = 0, top:[TopModel] = []) {
        self._total = total
        self.top = top
    }
}
