
import Foundation
import UIKit

class FavoritesModel {
    static let shared = FavoritesModel()
    var top : [TopModel]
    
    init() {
        top = []
    }
    
    func addItemInFavorites(_ item:TopModel) {
        FavoritesModel.shared.top.append(item)
    }
    
    func removeItemInFavorites(_ item:TopModel) {
        FavoritesModel.shared.top = FavoritesModel.shared.top.filter { $0.game.name != item.game.name }
    }
    
    func clear() {
        FavoritesModel.shared.top = []
    }
    
    func isEmpty() -> Bool {
        return FavoritesModel.shared.top.count == 0
    }
    
    func count() -> Int {
        return FavoritesModel.shared.top.count
    }
    
}

