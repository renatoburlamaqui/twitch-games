
import Foundation
import UIKit

class GameModel : Codable {
    var name: String
    var popularity: Int
    var _id: Int
    let giantbomb_id: Int
    var logo: LogoModel
    var box: LogoModel
    
    init() {
        self.name = ""
        self.popularity = 0
        self._id = 0
        self.giantbomb_id = 0
        self.logo = LogoModel()
        self.box = LogoModel()
    }
}
