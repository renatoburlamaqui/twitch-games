//
//  Favorites+CoreDataProperties.swift
//  Twitch Games
//
//  Created by Patrícia Pampanelli on 05/03/2018.
//  Copyright © 2018 Renato Medina. All rights reserved.
//
//

import Foundation
import CoreData


extension Favorites {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Favorites> {
        return NSFetchRequest<Favorites>(entityName: "Favorites")
    }

    @NSManaged public var id: Int32?
    @NSManaged public var name: String?
    @NSManaged public var popularity: Int32
    @NSManaged public var viewers: Int32
    @NSManaged public var logoSmallUrl: String?
    @NSManaged public var logolargeUrl: String?

}
