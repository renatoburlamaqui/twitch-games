import UIKit
import Foundation

class LogoModel: Codable {
    var large : String
    var medium : String
    var small : String
    var template : String
    
    init() {
        self.large = ""
        self.medium = ""
        self.small = ""
        self.template = ""
    }
}
