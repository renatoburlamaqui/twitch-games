
import Foundation
import UIKit

class TopModel : Codable {
    var game : GameModel
    var viewers: Int
    var channels: Int
    
    init() {
        self.game = GameModel()
        self.viewers = 0
        self.channels = 0
    }
    
    func hasFavoritted() -> Bool {
        return FavoritesModel.shared.top.contains(where: { $0.game.name == self.game.name })
    }
}
